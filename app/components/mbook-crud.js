import Component from '@ember/component';

export default Component.extend({
	actions:{
		guardar(){
			this.onGuardar();
		},
		eliminar(){
			this.onEliminar();
		}
	}
});
