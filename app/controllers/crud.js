import Controller from '@ember/controller';

export default Controller.extend({
	actions:{
		onGuardar(mbook){
			mbook.save().then(()=>{
				alert("Libro guardado");
			});
		},
		onEliminar(mbook){
			mbook.destroyRecord().then(()=>{
				alert("Libro eliminado");
			})
		},
		nuevo(){
			this.store.createRecord('music-book');
		}
	}
});
